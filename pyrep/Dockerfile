FROM nvidia/cuda:11.2.2-cudnn8-runtime-ubuntu18.04

ENV DEBIAN_FRONTEND="noninteractive"

ARG UID=10267
ARG GID=1000
ARG GNAME=recod
ARG USERNAME=rafael.prudencio

# Setup group and username
RUN if [ $GNAME != users ]; then groupadd --gid $GID $GNAME; fi && \
    useradd --create-home --shell /bin/bash --uid $UID --gid $GID $USERNAME && \
    echo "$USERNAME:$USERNAME" | chpasswd && \
    adduser $USERNAME sudo

# Set locale
RUN apt-get update && apt-get install -y \
	software-properties-common \
	locales
RUN locale-gen en_US.UTF-8
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US:en
ENV LC_ALL en_US.UTF-8

# Install programs
RUN apt-get install -y \
	curl \
	wget \
	git \
	vim \
	screen \
	make \
	build-essential \
	xz-utils

# Install Python
RUN add-apt-repository ppa:deadsnakes/ppa -y
RUN apt-get update & apt-get install -y \
	python3.8 \
	python3.8-dev \
	python3-pip

# Install CoppeliaSim
# Based on official Dockerfile: https://github.com/CoppeliaRobotics/docker-image-coppeliasim
RUN apt-get install -y \
	libx11-6 \
	libxcb1 \
	libxau6 \
	libgl1-mesa-dev \
	xvfb \
	dbus-x11 \
	x11-utils \
	libxkbcommon-x11-0 \
	libavcodec-dev \
	libavformat-dev \
	libswscale-dev

ENV COPPELIASIM_ROOT=/home/$USERNAME/coppeliasim
ENV COPPELIASIM_FILENAME=CoppeliaSim_Edu_V4_2_0_Ubuntu18_04.tar.xz
ENV LD_LIBRARY_PATH=$COPPELIASIM_ROOT:$LD_LIBRARY_PATH
ENV QT_QPA_PLATFORM_PLUGIN_PATH=$COPPELIASIM_ROOT

RUN wget https://coppeliarobotics.com/files/$COPPELIASIM_FILENAME
RUN tar -xf $COPPELIASIM_FILENAME
RUN rm $COPPELIASIM_FILENAME
RUN mv $(basename $COPPELIASIM_FILENAME .tar.xz) $COPPELIASIM_ROOT

EXPOSE 19997

# Install PyRep
RUN apt-get install -y \
	libffi-dev
RUN python3.8 -m pip install cffi Cython numpy
RUN python3.8 -m pip install git+https://github.com/stepjam/PyRep.git@d778d5d4ffa3be366d4e699f6e2941553fd47ecc
